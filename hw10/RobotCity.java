//Xinchen Ma
//April. 24  2018
//CSE02
/*
* RobotCity: utilize the multidimensional arrays
*/


public class RobotCity {
	
	//Create build City method
	public static int[][] buildCity(){
		int heigth = (int)(Math.random() * 5 + 10);
		int width = (int)(Math.random() * 5 + 10);
		int[][] city = new int[heigth][width];
		for(int i = 0; i < heigth; i++) {
			for(int j = 0; j < width; j++) {
				city[i][j] = (int)(Math.random() * 899 + 100);
			}
		}
		
		return city;
	}
	
	//display the array
	public static void display(int[][] a){
		for(int i = 0; i < a.length; i++) {
			for(int j = 0; j < a[i].length; j++) {
				System.out.print(a[i][j] + "\t\t");
			}
			System.out.println();
		}
	}
	
	//invade: put random number of robots in the random coordinates, 
	//two robots should not appear in the same block
	public static int[][] invade(int[][] a, int k){
		int index = k;
		int[][] result = a;
		while(index > 0) {
			int randomHeight = (int)(Math.random() * a.length);
			int randomWidth = (int)(Math.random() * a[0].length);
			if(result[randomHeight][randomWidth] > 0) {
				result[randomHeight][randomWidth] = -1 * result[randomHeight][randomWidth];
				index--;
			}
		}
		return result;
	}
	
	//update: move all the robots to the right one block
	public static int[][] update(int[][] a){
		int[][] result = a;
		for(int i = 0; i < result.length; i++) {
			for(int j = 0; j < result[i].length - 1; j++) {
				if(result[i][j] < 0 && result[i][j + 1] > 0) {
					result[i][j + 1] = -1 * result[i][j + 1];
					j++;
				}
			}
		}
		return result;
	}
	
	//main method
	public static void main(String[] args) {
		int[][] city = buildCity();
		display(city);
		System.out.println();
		int numOfRobots = (int)(Math.random() * 50);
		city = invade(city, numOfRobots);
		display(city);
		System.out.println();

		for(int i = 0; i < 5; i++) {
			city = update(city);
			display(city);
			System.out.println();
		}//end of for loop
	}//end of main method
}//end of class