//Xinchen Ma
//April 13, 2018
//CSE02
//student grade: creat two arrays. One is student name which read by the user input
//the other one is grade. It creats random number between 0 ~ 100
import java.util.Scanner;

class StudentGrade{
  public static void main(String[] args){
    Scanner myScan = new Scanner(System.in);
    int noOfStudents = (int)(Math.random() * 5 + 5);
    System.out.println("Enter " + noOfStudents + " student names: ");
    
    String[] studentName = new String[noOfStudents];
    for(int i = 0; i < noOfStudents; i++){
      studentName[i] = myScan.nextLine();
    }//end of for
    
    int[] grades = new int[noOfStudents];
    for(int i = 0; i < noOfStudents; i++){
      grades[i] = (int)(Math.random() * 100);
    }//end of for
    
    System.out.println("\nHere are the midterm grades of the " + noOfStudents + " students above: ");
    for(int i = 0; i < noOfStudents; i++){
      System.out.println(studentName[i] + " : " + grades[i]);
    }//end of for
  }
}