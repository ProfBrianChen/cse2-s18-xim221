// Xinchen Ma
// Feb. 16 2018
// CSE02
// CardGenerator: pick a card random and print it out


public class CardGenerator{
  public static void main(String[] args){
   
    //random pick a number
    int cardNumber = (int)(Math.random() * 51) + 1;
    

    
    //give the suit name to the String
    String suitName = "";
    int cardName;
    
    if (cardNumber <= 13){
      suitName = "Diamonds";
      cardName = cardNumber;
      
    }//end of diamonds
    else if (cardNumber <= 26) {
      suitName = "Clubs";
      cardName = cardNumber - 1 * 13;
      
    }//end of clubs
    else if (cardNumber<= 39){
      suitName = "Hearts";
      cardName = cardNumber - 2 * 13;
      
    }//end of hearts
    else {
      suitName = "Spades";
      cardName = cardNumber - 3 * 13;
    }//end of spades
    
    
    
    //find the name of the cardSuit
    String cardNameString = "";
    switch (cardName){      
      case 1:
        cardNameString = "Ace";
        break;
        
      case 11: 
        cardNameString = "Jack";
        break;
      
      case 12:
        cardNameString = "Queen";
        break;
        
      case 13:
        cardNameString = "King";
        break;
      
      default:
        cardNameString = "" + cardName;
    }//end of switch
    
    //print the result
    System.out.println("You picked the " + cardNameString + " of " + suitName);
    
  }//end of main method
}//end of the class