//Xinchen Ma
//April. 17  2018
//CSE02
/*
 * Hw09:create an array for a deck of cards
 * Implement a draw poker game with exposed hands.  
 * Assume there are two players.
 *  You'll need to implement a shuffle function to shuffle a deck of 52 cards represented as an array of 52 integer members. 
 * Next, generate hands for both players.  
 * Draw the cards as the dealer would - the first card goes to the first player, the second to the second player
 * , the third to the first player, and so on, until all hands are full.
 * Finally, decide which hand wins. 
 * To do so, implement methods that evaluate a hand for containing a particular poker hand:
 * 		 pair() accepts an array of ints (5 ints, describing a hand) as input, and it returns boolean as output.  
 * It should return true if the hand contains a pair, and false otherwise. 
 *  Do this for three of a kind, flush, and full house.  
 *  Everything else is assumed to be a high card hand.  
 *  A high card hand is defined by the highest card in the hand. 
 */

public class DrawPoker {
	public static void main(String[] args) {

		//create a new deck of cards
		int[] deck = new int[52];
		for(int i = 0; i < 52; i++) {
			deck[i] = i;
		}

		//shuffle
		deck = shuffle(deck);

		//generate card for players
		int[] playerA = new int[5];
		int[] playerB = new int[5];

		for(int i = 0; i < 10; i++) {
			if(i % 2 == 0) {
				playerA[i / 2] = deck[i];
			}
			else {
				playerB[i / 2] = deck[i];
			}
		}
		//create name of the card
		String[] suit = {"diamond","clubs","hearts","spades"};
		String[] cardName = {"Ace","2","3","4","5","6","7","8","9","10","jack","queen","king"};
		//print out the cards each player have
		System.out.println("PlayerA has: ");
		for(int i = 0; i < 5; i++) {
			System.out.print(playerA[i] + "   " + cardName[playerA[i] % 13] + " of " + suit[playerA[i] / 13] + "     ");
		}
		System.out.println();
		
		System.out.println("PlayerB has: ");
		for(int i = 0; i < 5; i++) {
			System.out.print(playerB[i] + "   " + cardName[playerB[i] % 13] + " of " + suit[playerB[i] / 13] + "     ");
		}
		System.out.println();
		
		
		//start comparing
		boolean AHasPair = pair(playerA);
		boolean BHasPair = pair(playerB);
		boolean AHasThreeOfAKind = threeOfAKind(playerA);
		boolean BHasThreeOfAKind = threeOfAKind(playerB);
		boolean AHasFlush = flush(playerA);
		boolean BHasFlush = flush(playerB);
		boolean AHasFullHouse = fullHouse(playerA);
		boolean BHasFullHouse = fullHouse(playerB);
		
		int valueOfA = -1;
		if(AHasFullHouse) {
			valueOfA = 100000;
		}
		else if(AHasFlush) {
			valueOfA = 10000;
		}
		else if(AHasThreeOfAKind) {
			valueOfA = 1000;
		}
		else if(AHasPair) {
			valueOfA = 100;
		}
		else {
			valueOfA = highCardHand(playerA);
		}
		
		int valueOfB = -1;
		if(BHasFullHouse) {
			valueOfB = 100000;
		}
		else if(BHasFlush) {
			valueOfB = 10000;
		}
		else if(BHasThreeOfAKind) {
			valueOfB = 1000;
		}
		else if(AHasPair) {
			valueOfB = 100;
		}
		else {
			valueOfB = highCardHand(playerB);
		}
		
		if(valueOfA > valueOfB) {
			System.out.println("Player A win!!!");
		}
		else if(valueOfB > valueOfA) {
			System.out.println("Player B win!!!");
		}
		else {
			System.out.println("Tie.");
		}
	}
		


		//shuffle: shuffle the original array and return the shuffled one
		public static int[] shuffle(int[] a) {
			int[] original = a;
			int[] shuffled = new int[52];
			for(int i = 0; i < 52; i++) {
				int index = (int)(Math.random() * 52);
				if(original[index] >= 0) {
					shuffled[i] = original[index];
					original[index] = -1;
				}
			}
			return shuffled;
		}//end of method

		//pair: return true if there is a pair, false if there isn't. 
		public static boolean pair(int[] a) {
			int[] original = new int[5];
			for(int i = 0; i < 5; i++) {
				original[i] = a[i] % 13;
			}
			int numberOfSame = 0;
			int valueOfTheSameCard = -1;
			for(int i = 0; i < 4;i++) {
				for(int j = i + 1; j < 5; j++) {
					if(original[i] == original[j]) {
						numberOfSame += 1;
						if(valueOfTheSameCard == -1) {
							original[i] = valueOfTheSameCard;
						}
						else if(original[i] != valueOfTheSameCard) {
							numberOfSame -= 1;
						}
					}
				}
			}
			if(numberOfSame == 1) {
				return true;
			}
			else {
				return false;
			}
		}//end of method

		//three of a kind: return true if there is a three of a kind
		public static boolean threeOfAKind(int[] a) {
			int[] original = new int[5];
			for(int i = 0; i < 5; i++) {
				original[i] = a[i] % 13;
			}
			int numberOfSame = 0;
			for(int i = 0; i < 4;i++) {
				for(int j = i + 1; j < 5; j++) {
					if(original[i] == original[j]) {
						numberOfSame += 1;
					}
				}
			}
			if(numberOfSame == 2) {
				return true;
			}
			else {
				return false;
			}
		}//end of method

		//flush: if the all the cards are in the same suit, return true,
		public static boolean flush(int a[]) {
			int[] original = new int[5];
			for(int i = 0; i < 5; i++) {
				original[i] = a[i] / 13;
			}
			for(int i = 0; i < 5; i++) {
				int comparison = original[1];
				if(original[i] != comparison) {
					return false;
				}
			}
			return true;
		}//end of method

		public static boolean fullHouse(int a[]) {
			int[] original = new int[5];
			int numberOfSame = 0;
			int valueOfTheSameCard = -1;
			for(int i = 0; i < 5; i++) {
				original[i] = a[i] % 13;
			}
			for(int i = 0; i < 4;i++) {
				for(int j = i + 1; j < 5; j++) {
					if(original[i] == original[j]) {
						numberOfSame += 1;
						if(valueOfTheSameCard == -1) {
							original[i] = valueOfTheSameCard;
						}
						else if(numberOfSame == 2) {
							original[i] = valueOfTheSameCard;
						}
					}
				}
			}
			for(int i = 0; i < 5; i++) {
				if(original[i] == valueOfTheSameCard) {
					original[i] = -100;
				}
			}

			int valueOfSecondPair1 = -1;
			int valueOfSecondPair2 = -1;
			for(int i = 0; i < 5; i++) {
				if(a[i] != -100 && valueOfSecondPair1 == -1) {
					valueOfSecondPair1 = a[i];
					a[i] = -100;
				}
				if(a[i] != 100 && valueOfSecondPair1 != -1){
					valueOfSecondPair2 = a[i];
				}
			}
			if(valueOfSecondPair1 == valueOfSecondPair2) {
				return true;
			}
			else{
				return false;
			}
		}//end of method
		
		//high card hand: A high card hand is defined by the highest card in the hand.
		public static int highCardHand(int[] a) {
			int[] original = a;
			for(int i = 0; i < 5; i++) {
				original[i] = a[i] % 13;
			}
			int highest = -1;
			for(int i = 0; i < 5; i++) {
				if(original[i] > highest) {
					highest = original[i];
				}
			}
			return highest;
		}
	}