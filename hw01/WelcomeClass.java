///////////////////////////////////
///////CSE 2 Welcom Class
//////
public class WelcomeClass{
  
  public static void main(String[] args){
    //Print welcom sign to the terminal window
    
    System.out.println("    -----------");
    System.out.println("   | WELCOME |");
    System.out.println("    -----------");
    System.out.println("  ^ ^  ^ ^  ^ ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println(" <-X---I---M---2---2---1->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v v  v v  v v");
                       
  }// end of main method
}// end of the class