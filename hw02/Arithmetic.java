// Xinchen Ma
// Feb. 5 2018
// CSE02
// Arithmetic: Calculating numbers
public class Arithmetic{

  public static void main(String args[]){
  
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;

    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;

    //Number of belts
    int numBelts = 1;
    //cost per box of envelopes
    double beltCost = 33.99;

    //the tax rate
    double paSalesTax = 0.06;
    
    double totalCostOfPants, totalCostOfShirts, totalCostOfBelts;   //The total cost of each item
    double taxOfPants, taxOfShirts, taxOfBelts;                     //The sale tax of each item
    double totalCost, totalTax, totalCostAfterTax;                  //The total cost in all, the sum of all the tax, and the toatl cost after tax
    
    //The total cost of pants, shirts, and belts
    totalCostOfPants = numPants * pantsPrice;
    totalCostOfShirts = numShirts * shirtPrice;
    totalCostOfBelts = numBelts * beltCost;
    
    //The tax of each item
    taxOfPants = totalCostOfPants * paSalesTax;
    taxOfShirts = totalCostOfShirts * paSalesTax;
    taxOfBelts = totalCostOfBelts * paSalesTax;
    
    totalCost = totalCostOfBelts + totalCostOfShirts + totalCostOfPants;  //The total cost of purchases

    //Eliminate extra digits
    taxOfBelts = (int)(taxOfBelts * 100)/100.0;
    taxOfShirts = (int)(taxOfShirts * 100)/100.0;
    taxOfPants = (int)(taxOfPants * 100)/100.0;
    
    
    totalTax = taxOfBelts + taxOfShirts + taxOfPants;                     //The total sale tax
    totalTax = (int)(totalTax * 100)/100.0;                               //Eliminalte extre digits
    totalCostAfterTax = totalCost + totalTax;                             //The total paid for this transaction, including sales tax
    
    //Print out the total cost  and tax of each item, the total cost of purchases, the total sale tax and the total paid for this transaction
    System.out.println("The Cost of Pants is $" + totalCostOfPants + " and the sale tax is $" + taxOfPants);
    System.out.println("The Cost of Shirts is $" + totalCostOfShirts + " and the sale tax is $" + taxOfShirts);
    System.out.println("The Cost of Belts is $" + totalCostOfBelts + " and the sale tax is $" + taxOfBelts);
    System.out.println("The total cost of purchases is $" + totalCost + " and the total sale tax is $" + totalTax);
    System.out.println("The total paid for this transaction, including sales tax is $" + totalCostAfterTax);
    
  }//end of main method
}//end of class