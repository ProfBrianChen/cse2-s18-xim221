// Xinchen Ma
// Feb. 2 2018
// CSE02
// Cyclometer: calculate the time and distance
public class Cyclosmeter{
  public static void main(String[] args){
    int secsTrip1 = 480;    // 1st trip time
    int secsTrip2 = 3220;   // 2nd trip time
    int countsTrip1 = 1561; // 1st trip counts of rotations
    int countsTrip2 = 9037; // 2nd trip counts of rotations
    
    double wheelDiameter = 27.0;  // The diameter of the whell
    double PI = 3.14159;              // the Pi value
    int feetPerMile = 5280;          // feet per mile
    int inchesPerFoot = 12;           // inches per foot
    double secondsPerMinute = 60;        // seconds per minute
    double distanceTrip1, distanceTrip2, totalDistance;   // create the vari of distance of trip1,2 ant total
   
    
    //print the time and counts for each trip
    System.out.println("Trip 1 took " + (secsTrip1 / secondsPerMinute) + 
                      " minutes and had " + countsTrip1 + " counts. ");
    System.out.println("Trip 2 took " + (secsTrip2 / secondsPerMinute) + 
                      " minutes and had " + countsTrip2 + " counts. ");
    
    
    distanceTrip1 = countsTrip1 * wheelDiameter * PI;   // trip 1 distance in inches
    distanceTrip1 /= inchesPerFoot * feetPerMile;       // change into miles
    distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile;  // trip 2 distance in miles
    totalDistance = distanceTrip1 + distanceTrip2;      // the totaldistance of trip 1 and 2
    
    //print the distance and total distance
    System.out.println("Trip 1 was " + distanceTrip1 + " miles");
    System.out.println("Trip 2 was " + distanceTrip2 + " miles");
    System.out.println("The total dsitance was " + totalDistance + " miles");
  }   // end of main method
}   // end of class