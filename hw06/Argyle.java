//Xinchen Ma


//March 9, 2018
//CSE02
/* Argyle: print the argyle in the windows. Users need print
 *  width, length, characters filled in.
 */

import java.util.Scanner;

public class Argyle {
	public static void main(String[] args) {


		//import Scanner class(create new object)
		Scanner myScan = new Scanner(System.in);

		//initiate variables
		int width = -1;
		int height = -1;
		int widthOfDimonds = -1;
		int widthOfStrips = -1;
		String test = "";

		//determine the type of input, only integers will be accepted

		//width
		boolean determine = true;
		while(determine){
			System.out.print("Please enter a positive integer for the width of Viewing window in characters: ");
			if(!myScan.hasNextInt()){
				determine = true;
				String junkWord = myScan.next();
				System.out.println("This is not a integer.");
			}
			else if((width = myScan.nextInt()) < 1){
				determine = true;
				System.out.println("This is not a positive integer.");
			}//end of elseif
			else{
				determine = false;  
			}//end of else
		}//end of while (width)

		determine = true;
		//height
		while(determine){
			System.out.print("Please enter a positive integer for the height of Viewing window in characters: ");
			if(!myScan.hasNextInt()){
				determine = true;
				String junkWord = myScan.next();
				System.out.println("This is not a integer.");
			}//end of if
			else if((height = myScan.nextInt()) < 1){
				determine = true;
				System.out.println("This is not a positive integer.");
			}//end of elseif
			else{
				determine = false;  
			}//end of else
		}//end of while (height)

		determine = true;
		//width of dimonds
		while(determine){
			System.out.print("Please enter a positive integer for the width of the argyle diamonds: ");
			if(!myScan.hasNextInt()){
				determine = true;
				String junkWord = myScan.next();
				System.out.println("This is not a integer.");
			}//end of if
			else if((widthOfDimonds = myScan.nextInt()) < 1){
				determine = true;
				System.out.println("This is not a positive integer.");
			}//end of elseif
			else{
				determine = false;  
			}//end of else
		}//end of while (width of dimonds)


		determine = true;
		//width of strips
		while(determine){
			System.out.print("Please enter a positive odd integer for the width of teh argyle center stripe: ");
			if(!myScan.hasNextInt()){
				determine = true;
				String junkWord = myScan.next();
				System.out.println("This is not a integer.");
			}//end of if
			else if((widthOfStrips = myScan.nextInt()) < 1){
				determine = true;
				System.out.println("This is not a positive integer.");
			}//end of elseif
			else if(widthOfStrips % 2 != 1){
				determine = true;
				System.out.println("This is not a odd integer.");
			}//end of elseif
			else if(widthOfStrips >= (widthOfDimonds + 1) / 2) {
				determine = true;
				System.out.println("The width of strip should not larger than a half of the width of the dimond");
			}
			else{
				determine = false;  
			}//end of else
		}//end of while (width)
		//record the input of characters
		System.out.print("Please enter a first character for the pattern fill: ");
		String temp = myScan.next();
		char character1 = temp.charAt(0);

		System.out.print("Please enter a second character for the pattern fill: ");
		temp = myScan.next();
		char character2 = temp.charAt(0);

		System.out.print("Please enter a third character for the stripe fill: ");
		temp = myScan.next();
		char character3 = temp.charAt(0);
		
		
		//end of type in
		////////////////////////////////////////////////////////////////////////////////
		//print out the argyle
		//divide it into parts, a diamond will be one part, then print this area several times
		
		//calculate how many rows of diamond it has
		int partsOfRows = height / (2 * widthOfDimonds);
		//calculate if there are incomplete rows of diamond, if it has, calculate how many rows this incomplete rows of diamond have
		int extraRows = height % (2 * widthOfDimonds);
		
		//start the 1st loop1, first loop control the part of rows
		for(int partOfRows = 0; partOfRows < partsOfRows; partOfRows++) {
			
			//calculate in a part of row, how many parts diamond this row has
			int partsOfColumns = width / (widthOfDimonds * 2);
			//calculate if there is a incomplete diamond, if it has, calculate how many columns this diamond has
			int extraColumns = width % (widthOfDimonds * 2);
			
			//start the 2nd loop1, this loop control one row
			for(int i = 0; i <= widthOfDimonds * 2; i++) {
				
				//start the 3rd loop1, this loop control the part of colunm
				for(int partOfColumns = 0; partOfColumns < partsOfColumns; partOfColumns++) {
					//start the 4th loop, this loop control a specific character
					for(int j = 0; j <= widthOfDimonds * 2; j++) {
						
						//in this loop, find what to print in this position
						int range = (widthOfStrips - 1) / 2;
						if(i <= widthOfDimonds) {
							if((i - range) <= j && j <= (i + range) || (i - range) <= (widthOfDimonds * 2 - j) && (widthOfDimonds * 2 - j) <= (i + range)) {
								System.out.print(character3);
							}
							else if(widthOfDimonds - i <= j && j <= widthOfDimonds + i) {
								System.out.print(character2);
							}
							else {
								System.out.print(character1);
							}
						}
						else {
							if((i - range) <= j && j <= (i + range) || (i - range) <= (widthOfDimonds * 2 - j) && (widthOfDimonds * 2 - j) <= (i + range)) {
								System.out.print(character3);
							}
							else if(i - widthOfDimonds <= j && j <= 2 * widthOfDimonds - (i - widthOfDimonds)) {
								System.out.print(character2);
							}
							else {
								System.out.print(character1);
							}
						}//end of if statement
					}//end of 4th loop
				}//end of 3rd loop1
				
				//this 3rd loop2 control the incomplete columns
				for(int j = 0; j <= extraColumns; j++) {
					int range = (widthOfStrips - 1) / 2;
					if(i <= widthOfDimonds) {
						if((i - range) <= j && j <= (i + range) || (i - range) <= (widthOfDimonds * 2 - j) && (widthOfDimonds * 2 - j) <= (i + range)) {
							System.out.print(character3);
						}
						else if(widthOfDimonds - i <= j && j <= widthOfDimonds + i) {
							System.out.print(character2);
						}
						else {
							System.out.print(character1);
						}
					}
					else {
						if((i - range) <= j && j <= (i + range) || (i - range) <= (widthOfDimonds * 2 - j) && (widthOfDimonds * 2 - j) <= (i + range)) {
							System.out.print(character3);
						}
						else if(i - widthOfDimonds <= j && j <= 2 * widthOfDimonds - (i - widthOfDimonds)) {
							System.out.print(character2);
						}
						else {
							System.out.print(character1);
						}
					}//end of if statement
				}//end of 3rd loop2
				System.out.println();
			}//end of 2nd loop1
		}//end of 1st loop1


		//this 1st loop2 controls the incomplete rows
		for(int i = 0; i <= extraRows; i++) {
			int partsOfColumns = width / (widthOfDimonds * 2);
			int extraColumns = width % (widthOfDimonds * 2);
			
			//2nd loop2
			for(int partOfColumns = 0; partOfColumns < partsOfColumns; partOfColumns++) {
				//3rd loop3
				for(int j = 0; j <= widthOfDimonds * 2; j++) {
					int range = (widthOfStrips - 1) / 2;
					if(i <= widthOfDimonds) {
						if((i - range) <= j && j <= (i + range) || (i - range) <= (widthOfDimonds * 2 - j) && (widthOfDimonds * 2 - j) <= (i + range)) {
							System.out.print(character3);
						}
						else if(widthOfDimonds - i <= j && j <= widthOfDimonds + i) {
							System.out.print(character2);
						}
						else {
							System.out.print(character1);
						}
					}
					else {
						if((i - range) <= j && j <= (i + range) || (i - range) <= (widthOfDimonds * 2 - j) && (widthOfDimonds * 2 - j) <= (i + range)) {
							System.out.print(character3);
						}
						else if(i - widthOfDimonds <= j && j <= 2 * widthOfDimonds - (i - widthOfDimonds)) {
							System.out.print(character2);
						}
						else {
							System.out.print(character1);
						}
					}//end of if statement
				}//end of 3rd loop3
			}//end of 2nd loop2
			
			//2nd loop3
			for(int j = 0; j <= extraColumns; j++) {
				int range = (widthOfStrips - 1) / 2;
				if(i <= widthOfDimonds) {
					if((i - range) <= j && j <= (i + range) || (i - range) <= (widthOfDimonds * 2 - j) && (widthOfDimonds * 2 - j) <= (i + range)) {
						System.out.print(character3);
					}
					else if(widthOfDimonds - i <= j && j <= widthOfDimonds + i) {
						System.out.print(character2);
					}
					else {
						System.out.print(character1);
					}
				}
				else {
					if((i - range) <= j && j <= (i + range) || (i - range) <= (widthOfDimonds * 2 - j) && (widthOfDimonds * 2 - j) <= (i + range)) {
						System.out.print(character3);
					}
					else if(i - widthOfDimonds <= j && j <= 2 * widthOfDimonds - (i - widthOfDimonds)) {
						System.out.print(character2);
					}
					else {
						System.out.print(character1);
					}
				}//end of if statement
			}//end of 2nd loop3
			System.out.println();
		}//end of 1st loop2
	}//end of main method
}//end of class