//Xinchen Ma
//CSE02
//April 13, 2018
/*Passing array: three method: copy, invert, inver2
*copy: copy the array
*invert: invert the array inside the method
*invert2: inver the array inside the method and return a new array
*/

class PassingArrays{
  public static int[] copy(int[] a){
    int[] result = new int[a.length];
    for(int i = 0; i < a.length; i++){
      result[i] = a[i];
    }
    return result;
  }
  
  public static void inverter(int[] a){
    int transit = -1;
    int halfOfLength = (int)(a.length / 2);
    for(int i = 0; i < halfOfLength - 1; i++){
      transit = a[i];
      a[i] = a[a.length - 1 - i];
      a[a.length  - 1 - i] = transit;
    }
  }
  
  public static int[] inverter2(int[] a){
    int[] result = copy(a);
    int transit = -1;
    int halfOfLength = (int)(result.length / 2);
    for(int i = 0; i < halfOfLength - 1; i++){
      transit = result[i];
      result[i] = result[result.length - 1 - i];
      result[result.length  - 1 - i] = transit;
    }
    return result;
  }
  
  public static void print(int[] a){
    System.out.println("This array is: ");
    for(int i = 0; i < a.length; i++){
      System.out.print(a[i] + " ");
    }
    System.out.println();
  }
  
    
    public static void main(String[] args){
      int[] array0 = {3,5,2,4,7,4,8,2,4,7,44};
      int[] array1 = copy(array0);
      int[] array2 = copy(array0);
      
      inverter(array0);
      print(array0);
      inverter2(array1);
      print(array1);
      int[] array3 = inverter2(array2);
      print(array3);
    }
}