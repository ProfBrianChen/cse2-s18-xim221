//Xinchen Ma
//March 9, 2018
//CSE02
/* encrypted_x.java: print out a hidden "X" in the terminal, 
the user will choose the size of the square */

//import Scanner class
import java.util.Scanner;

class encrypted_x{
  public static void main(String[] args) {
    Scanner myScan = new Scanner(System.in); //create a new object in Scanner class
    
    int size = -1; //initiate size
    String test = ""; //initiate test
    
    //detect what users typed in: only integers will be accepted
    boolean determine = true;
    while(determine){
      System.out.print("Please type the size of the square(0~100): ");
      if(!myScan.hasNextInt()){
        determine = true;
        String junkWord = myScan.next();
        System.out.println("This is not a integer.");
      }//end of if
      else if((size = myScan.nextInt()) < 1 || size > 100){
        determine = true;
        System.out.println("This integer is out of range 0~100, please choose another one.");
      }//end of elseif
      else{
        determine = false;  
      }//end of else
    }//end of while 
    
    //start the for loop, the first loop will print the row
    for(int i = 0; i <= size; i++){
      
      //inner loop will print each character in a row
      for(int j = 0; j <= size; j++){
       //if statement will determine if the character should be changed form "*" to " "
        if(j == i || j == size - i){
          System.out.print(" ");
        }
        else{
          System.out.print("*");
        }//end of if statement
      }//end of inner for loop
      System.out.println();
    }//end of the first for loop
  }//end of main method
}//end of class