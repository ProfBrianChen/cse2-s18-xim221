//Xinchen Ma
//April. 20
//cse002
//lab 10: operation to the 2d array: row-major to column-major, adding

public class TwoDArray{
  public static int[][] increasingMatrix(int width, int height, boolean format){
    int[][] a = new int[width][height];
    
    //form the row-major array
    if(format){
      int count = 1;
      for(int i = 0; i < width; i++){
        for(int j = 0; j < height; j++){
          a[i][j] = count;
          count++;
        }
      }//end of the loops
    }//end of if
    
    //form the column-major array
    else{
      int count = 1;
      for(int i = 0; i < width; i++){
        for(int j = 0; j < height; j++){
          a[j][i] = count;
          count++;
        }
      }//end of the loops
    }//end of if
    return a;
  }//end of this method
  
  public static void printMatrix(int[][] array, boolean format){
    //form the row-major array
    if(format){
      for(int i = 0; i < array.length; i++){
        for(int j = 0; j < array[0].length; j++){
          System.out.print(array[i][j] + " ");
        }
        System.out.println();
      }//end of the loops
    }//end of if
    else{
      int count = 1;
      for(int i = 0; i < array.length; i++){
        for(int j = 0; j < array[0].length; j++){
         System.out.print(array[j][i] + " ");
        }
        System.out.println();
      }//end of the loops
    }//end of if
  }//end of this methos
  
  public static int[][] translate(int[][] array){
    int a[][] = new int[array[0].length][array.length];
    for(int i = 0; i < array[0].length; i++){
      for(int j = 0; j < array.length; j++){
        a[i][j] = array[j][i];
      }
    }//end of for
    return a;
  }//end of this method
  
  public static int[][] addMatrix(int[][] a, boolean formatA, int[][] b, boolean formatB){
    int widthA = a.length;
    int heightA = a[0].length;
    int widthB = b.length;
    int heightB = b[0].length;
    
    if(widthA != widthB || heightA != heightB){
      System.out.println("The arrays connot be added!");
      return null;
    }//end of if
    else{
      if(formatA && formatB){
        int[][] c = new int[widthA][heightA];
        for(int i = 0; i < widthA; i++){
          for(int j = 0; j < heightB; j++){
            c[i][j] = a[i][j] + b[i][j];
          }
        }//end of the loops
        return c;
      }
      else if(!formatA && formatB){
        int[][] c = new int[widthB][heightB];
        for(int i = 0; i < widthA; i++){
          for(int j = 0; j < heightB; j++){
            c[i][j] = a[j][i] + b[i][j];
          }
        }//end of the loops
        return c;
      }
      else if(formatA && !formatB){
        int[][] c = new int[widthA][heightB];
        for(int i = 0; i < widthA; i++){
          for(int j = 0; j < heightB; j++){
            c[i][j] = a[j][i] + b[i][j];
          }
        }//end of the loops
        return c;
      }
      else if(!formatA && !formatB){
        int[][] c = new int[heightB][widthB];
        for(int i = 0; i < widthA; i++){
          for(int j = 0; j < heightB; j++){
            c[i][j] = a[j][i] + b[j][i];
          }
        }//end of the loops
        return c;
      }
      return c;
    }
  }//end of the method
  
  public static void main(String[] args){
    int widthA = (int)(Math.random() * 10);
    int heightA = (int)(Math.random() * 10);
    int widthB = (int)(Math.random() * 10);
    int heightB = (int)(Math.random() * 10);
    
    int[][] a = increasingMatrix(widthA, heightA, true);
    int[][] b = increasingMatrix(widthA, heightA, false);
    int[][] c = increasingMatrix(widthB, heightB, true);
    System.out.println("Generating a matrix with width" + widthA + " and height " + heightA + ": ");
    printMatrix(a, true);
    
    System.out.println("Generating a matrix with width" + widthA + " and height " + heightA + ": ");
    printMatrix(b, false);
    
    System.out.println("Generating a matrix with width" + widthB + " and height " + heightB + ": ");
    printMatrix(c, true);
    
    System.out.println("Adding two matrices, output:");
    printMatrix(addMatrix(a, true, b, false), true);
    
    addMatrix(a, true, c, true);
  }//end of main method
}