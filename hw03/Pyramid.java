// Xinchen Ma
// Feb. 12 2018
// CSE02
// Pyramid: calculate the volume of a pyramid

import java.util.Scanner; //import Scanner class

public class Pyramid{
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in); //accept input
    
    //read the square side of the pyramid and give the value to the variable
    System.out.print("The square side of the pyramid is (input length): ");
    double side = myScanner.nextDouble();
    
    //read the hight of the pyramid and give the value to the variable
    System.out.print("The hight of the pyramid is (input height): ");
    double height = myScanner.nextDouble();
    
    //calculate the volume
    double volume = Math.pow(side, 2.0) * height / 3;
    
    //print the result
    System.out.println("The volume inside the pyramid is : " + volume);
    
  }//end of the main method
}//end of class