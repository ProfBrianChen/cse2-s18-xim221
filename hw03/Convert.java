// Xinchen Ma
// Feb. 12 2018
// CSE02
// Convert: convert acres * inches to cubic miles

import java.util.Scanner; //import Scanner class

public class Convert{
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in); //accept input
    
    //read the acres of affected area from the user and give the value to the varaible
    System.out.print("Enter the affected area in acres: ");
    double areaInAcre = myScanner.nextDouble();
    
    //read the inches of rain drop from the user and give the value to the variable
    System.out.print("Enter the rainfall in the affected: ");
    double rainDrop = myScanner.nextDouble();
    
    //calculate the volume in Acre-inches
    double volumeAcreInches = areaInAcre * rainDrop;
    
    //convert the volumeAcreInches to cubic miles
    double volumeMiles = volumeAcreInches / 40550400;
    
    //print the result
    System.out.println(volumeMiles + " cubic miles");
    
  }//end of main method
}//end of class