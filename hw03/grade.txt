Grade: 98. 
Program compiles and runs without error, accepts appropriate input, and calculates results correctly. 
Nicely formatted and mostly effective use of comments throughout your program. 
However, comment at the top of your program explaining what the program will do could be more specific/meaningful. 
Also, based on your commenting, I would suggest that you refresh yourself on what exactly the statement Scanner myScanner = new Scanner(System.in); means. 
While you implemented the statement correctly, this statement itself does nothing to accept user input. While this did not detract from the functionality of your program, it's important to understand what each implemented statement actually does. 
Good work!