// Xinchen Ma
// Feb. 12 2018
// CSE02
// Yahtzee: A dice game which requied to find the sum of the points in a spcific situation

import java.util.Scanner; //import Scanner class
import java.util.Arrays;  //import Array class

class Yahtzee{
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in); //create a new object in Scanner class
    int a = 0, b = 0, c = 0, d = 0, e = 0; //initialize the five dices
    
    //read the result: "yes" or "no" from the user
    System.out.print("Do you want to roll the dice randomly? (Please type 'yes' or 'no'): ");
    String choice = myScanner.next();
    
    //judge the result and if user choose "yes": give five random numbers to the variables; if the choic is "no", ask the user for the numbers
    if(choice.equals("yes")){
      a = (int)(Math.random() * 5) + 1;
      b = (int)(Math.random() * 5) + 1;
      c = (int)(Math.random() * 5) + 1;
      d = (int)(Math.random() * 5) + 1;
      e = (int)(Math.random() * 5) + 1;
      
      //Print the random values of dices out
      System.out.println("The number of five dices are: " + a + ", " + b + ", " + c + ", " + d + ", " + e + ".");
    }//end of if state
    else if(choice.equals("no")){
      
      //read the number from the user
      System.out.print("Please type a number (1 to 6) for the 1st dice: ");
      a = myScanner.nextInt();
      
      //jdetermine if the number is impossible
      if(a < 1 || a > 6){
        System.out.println("This number is not from 1 to 6, the program will be ended.");
        System.exit(1);
      }//end of judging block
      
      
      //repeat the reading process listed above until variable "e" has a value
      //read the number from the user
      System.out.print("Please type a number (1 to 6) for the 2nd dice: ");
      b = myScanner.nextInt();
      
      //determine if the number is impossible
      if(b < 1 || b > 6){
        System.out.println("The number is not from 1 to 6, the program will be ended.");
        System.exit(1);
      }//end of judging block
      
      //read the number from the user
      System.out.print("Please type a number (1 to 6) for the 3rd dice: ");
      c = myScanner.nextInt();
      
      //determine if the number is impossible
      if(c < 1 || c > 6){
        System.out.println("This number is not from 1 to 6, the program will be ended.");
        System.exit(1);
      }//end of judging block
      
      //read the number from the user
      System.out.print("Please type a number (1 to 6) for the 4th dice: ");
      d = myScanner.nextInt();
      
      //determine if the number is impossible
      if(d < 1 || d > 6){
        System.out.println("This number is not from 1 to 6, the program will be ended.");
        System.exit(1);
      }//end of judging block
      
      //read the number from the user
      System.out.print("Please type a number (1 to 6) for the 5th dice: ");
      e = myScanner.nextInt();
      
      //determine if the number is impossible
      if(e < 1 || e > 6){
        System.out.println("This number is not from 1 to 6, the program will be ended.");
        System.exit(1);
      }//end of judging block
    }//end of the "no" situation
    
    //if the user neither type"yes" or "no", print a warning and exit
    else{
      System.out.println("Please type 'yes' or 'no', the program will be ended");
      System.exit(1);
    }//end of else state
    
    
    
    //The calculation of upper section: calculate sum of the roll by the value of each dice
    int sumOfUpper = a + b + c + d + e;
    int sumOfUpperBonus = sumOfUpper;
    if(sumOfUpper > 63){
      sumOfUpperBonus += 35;
    }
    
    
    //The calculation of lower section
    int sumOfLower = 0;
    
    //arrange the array
    int[] intArray = new int[] {a, b, c, d, e};
    Arrays.sort(intArray);
    
    //Yahtzee
    if(a == b && b == c && c == d && d == e){
      sumOfLower = 50;
    }
    
    //determine whethere the roll has 4 of a kind
    else if((a == b && b == c && c == d) || (a == b && b == c && c == e) || (b == c && c == d && d == e)){
      sumOfLower = 4 * b;
    }
    
    //determine whethere the roll has 3 of a kind
    else if( (a == b && b == c) || (a == b && b == d) || (a == b && b == e) || (a == c && c == d) || (a == c && c == e) || (a == d && d == e)){
      sumOfLower = 3 * a;
    }//end of "starting with a"
    
    else if((b == c && c == d) || (b == c && c == e) || (b == d && d == e)){
      sumOfLower = 3 * b;
    }//end of "starting with b"
    
    else if(c == d && d == e){
      sumOfLower = 3 * c;
    }//end of "starting with c"
    
    //full house
    else if ((a == b && c != d && d != e) || (a == c && b != d && d != e) || (a == d && b != c && d != e) || (a == e && b != c & c != e)){
      sumOfLower = 25;  
    }
    
    else if ((b == c && a != d & d != e) || (b == d && a != c && c != e) || (b == e && a != c && c != d) || (c == d && a != b && b != e) || (c == e && a != b && b != d) || (d == e && a != b && b != c)){
      sumOfLower = 25;
    }
    
    //find the lg. straight
    if(intArray[0] + 1 == intArray[1] && intArray[1] + 1 == intArray[2] && intArray[2] + 1 == intArray[3] && intArray[3]  + 1 == intArray[4]){
      sumOfLower = 40;
    }
    
    
    //find the Sm. straight

    
    if(intArray[0] + 1 == intArray[1] && intArray[1] + 1 == intArray[2] && intArray[2] + 1 == intArray[3] && intArray[3]  + 1 != intArray[4]){
      sumOfLower = 30;
    }
    
    //chance
    int chance = sumOfUpper;
    
    //final result of lower
    sumOfLower += chance;
    
    //Grand sum
    int grandSum = sumOfUpperBonus + sumOfLower;
    
    //print out the result
    System.out.println(" upper section initial total is " + sumOfUpper + "\n upper section total including bonus is " + sumOfUpperBonus + "\n lower section total is " + sumOfLower + "\n grand total is " + grandSum);
    
  }//end of main method
}//end of class