//Xinchen Ma
//March 27, 2018
//CSE02
/* StringAnalysis: to find is there a letter in a specific position or the whole string
 */

import java.util.Scanner;

import org.omg.CORBA.SystemException;

public class StringAnalysis {

	public static boolean CheckLetters(String a) {
		String input = a;
		int length = input.length();
		boolean result = false;
		for(int i = 0; i <= length; i++) {
			if(Character.isLetter(input.charAt(i))) {
				result = true;
			}
		}
		return result;
	}

	public static boolean CheckLetters(String a, int x) {
		String input = a;
		int position = x;
		boolean result = false;
		for(int i = 0; i < position; i++) {
			if(Character.isLetter(input.charAt(i))) {
				result = true;
			}
		}
		return result;
	}

	public static void main(String[] args) {
		boolean determine = true;
		boolean hasLetter = false;

		Scanner myScanner = new Scanner(System.in);
		
		//check the input
		while(determine) {
			System.out.print("Please type a string: ");
			String a = myScanner.next();
			System.out.print("Do you want to check a certain number of character? ('yes' or 'no'): ");
			String checkYesNo = myScanner.next();

			if(checkYesNo.equals("yes")) {
				int position = -1;
				System.out.print("Plest type the number of letters you want to check: ");
				if(!myScanner.hasNextInt()){
					determine = true;
					String junkWord = myScanner.next();
					System.out.println("This is not a integer.");
				}
				else if((position = myScanner.nextInt()) < 1){
					determine = true;
					System.out.println("This is not a positive integer.");
				}//end of elseif
				else{
					determine = false;  
					hasLetter = CheckLetters(a, position);
					if(hasLetter) {
						System.out.println("This string has letter");
					}
					else {
						System.out.println("This string does not have letter");
					}
				}
				determine = false;
			}
			else if(checkYesNo.equals("no")) {
				hasLetter = CheckLetters(a);
				if(hasLetter) {
					System.out.println("This string has letter");
				}
				else {
					System.out.println("This string does not have letter");
				}
				determine = false;
			}
		}
	}
}