//Xinchen Ma
//March 27, 2018
//CSE02
/* Area: Calculate the area of Rectangle, Triangle, and Circle. 
* All the variables will be read from user input.
*/

import java.util.Scanner;

public class Area {
	//create a scan object
	static Scanner myScan = new Scanner(System.in);

	static boolean determine = true;
	static double area = -1;
	
	/*check method: accept input from user, check if the input is correct. 
 If not, request use to type in again
	 */		
	public static void Checking() {
		while(determine){
			System.out.print("Please enter the type of Area you want to calculate: ");
			String a = myScan.next();

			if(a.equals("rectangle")) {
				Rectangle();
				determine = false;
			}
			else if(a.equals("triangle")) {
				Triangle();
				determine = false;
			}
			else if(a.equals("circle")) {
				Circle();
				determine = false;
			}
			else {
				System.out.println("Please choose 'rectangle', 'triangle', or 'circle' (without caps)");
				determine = true;
			}
		}//end of while
	}//end of method checking

	//Rectangle method, calculate the area of rectangle, 
	public static void Rectangle() {
		double length = -1;
		double width = -1;
		while(determine) {
			System.out.print("Please type the data for length: ");

			//try to find a double(first if)
			if(!myScan.hasNextDouble()){
				determine = true;
				String junkWord = myScan.next();
				System.out.println("This is not a double.");
			}
			else if((length = myScan.nextInt()) < 1){
				determine = true;
				System.out.println("This is not a positive double.");
			}//end of elseif
			else{
				determine = false;
			}
		}
		determine = true;

		while(determine) {
			System.out.print("Please type the data for width: ");	

			//try to find a double(second if)
			if(!myScan.hasNextDouble()){
				determine = true;
				String junkWord = myScan.next();
				System.out.println("This is not a double.");
			}
			else if((width = myScan.nextInt()) < 1){
				determine = true;
				System.out.println("This is not a positive double.");
			}//end of elseif
			else{
				determine = false;  

				//calculate the area
				area = width * length;
				System.out.print("The area is: " + area);
			}//the end of first if
		}
	}//end of Rectangle method

	//Triangle method, calculate the area of triangle
	public static void Triangle() {
		double height = -1;
		double width = -1;

		while(determine) {
			System.out.print("Please type the data for height: ");

			//try to find a double(height)
			if(!myScan.hasNextDouble()){
				determine = true;
				String junkWord = myScan.next();
				System.out.println("This is not a double.");
			}
			else if((height = myScan.nextInt()) < 1){
				determine = true;
				System.out.println("This is not a positive double.");
			}//end of elseif
			else{
				determine = false;
			}
		}

		determine = true;

		while(determine) {
			System.out.print("Please type the data for width: ");	
			//try to find a double(second if)
			if(!myScan.hasNextDouble()){
				determine = true;
				String junkWord = myScan.next();
				System.out.println("This is not a double.");
			}
			else if((width = myScan.nextInt()) < 1){
				determine = true;
				System.out.println("This is not a positive double.");
			}//end of elseif
			else{
				determine = false;  

				//calculate the area
				area = width * height / 2;
				System.out.println("The area is: " + area);
			}//end of first if

		}
	}//end of Triangle method


	//circle method, calculate the area of corcle
	public static void Circle() {
		while(determine) {
			double radius = -1;
			System.out.print("Please type the data for radius: ");

			//try to find a double
			if(!myScan.hasNextDouble()){
				determine = true;
				String junkWord = myScan.next();
				System.out.println("This is not a double.");
			}
			else if((radius = myScan.nextInt()) < 1){
				determine = true;
				System.out.println("This is not a positive double.");
			}//end of elseif
			else{
				determine = false;  

				area = Math.PI * Math.pow(radius, 2);
				System.out.println("The area is: " + area);
			}//end of if
		}
	}

	//main method
	public static void main(String[] args) {
		Checking();
	}
}
