//Xinchen Ma
//March 9, 2018
//CSE02
//Story: Generate sentences by methods
import java.util.Random;

class Story{
  public static String Adjectives(){
    int randomInt = ((int)(Math.random() * 9));
    String Adj = "";
    switch(randomInt){
      case 0:
        Adj = "quick";
        break;
      case 1:
        Adj = "green";
        break;
      case 2:
        Adj = "slow";
        break;
      case 3:
        Adj = "high";
        break;
      case 4:
        Adj = "boring";
        break;
      case 5:
        Adj = "shinny";
        break;
      case 6:
        Adj = "colorful";
        break;
      case 7:
        Adj = "black";
        break;
      case 8:
        Adj = "white";
        break;
      case 9:
        Adj = "fat";
        break;
    }
    return Adj;
  }
  
  public static String Subject(){
    int randomInt = (int)(Math.random() * 9);
    String subject = "";
    switch(randomInt){
      case 0:
        subject = "fox";
        break;
      case 1:
        subject = "rabbit";
        break;
      case 2:
        subject = "bird";
        break;
      case 3:
        subject = "ant";
        break;
      case 4:
        subject = "airplane";
        break;
      case 5:
        subject = "car";
        break;
      case 6:
        subject = "bike";
        break;
      case 7:
        subject = "bus";
        break;
      case 8:
        subject = "wheel";
        break;
      case 9:
        subject = "mouse";
        break;
    }
    return subject;
  }
  
  public static String Verb(){
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    String verb = "";
    switch(randomInt){
      case 0:
        verb = "passed over a";
        break;
      case 1:
        verb = "caought";
        break;
      case 2:
        verb = "punched the";
        break;
      case 3:
        verb = "ate";
        break;
      case 4:
        verb = "stared at";
        break;
      case 5:
        verb = "kicked";
        break;
      case 6:
        verb = "jumped over the";
        break;
      case 7:
        verb = "broke the";
        break;
      case 8:
        verb = "stood on the";
        break;
      case 9:
        verb = "killed the";
          break;
    }
    return verb;
  }
  
  public static String Obj(){
    int randomInt = (int)(Math.random() * 9);
    String obj = "";
    switch(randomInt){
      case 0:
        obj = "water";
        break;
      case 1:
        obj = "tree";
        break;
      case 2:
        obj = "dog";
        break;
      case 3:
        obj = "cat";
        break;
      case 4:
        obj = "classroom";
        break;
      case 5:
        obj = "can";
        break;
      case 6:
        obj = "street";
        break;
      case 7:
        obj = "window";
        break;
      case 8:
        obj = "sign";
        break;
      case 9:
        obj = "crossroad";
        break;
    }
    return obj;
  }
  
  public static String Thesis(){
    String sub = Subject();
    String a = "The " + Adjectives() + " " + sub + " "+ Verb() + " The " + Obj();
    System.out.println(a);
    return sub;
  }
  
  public static void SecondSentence(String a){
    String x = "";
    int randomInt = (int)(Math.random() * 10);
    if(randomInt < 5){
      x = a;
    }
    else{
      x = "it";
    }
    String result = x + " " + Verb() + " "+ Obj();
    System.out.println(result);
  }
  
  public static void Conclusion(String a){
    String x = a;
    String result = "That " + x + " " + Verb() + " "+ Obj();
    System.out.println(result);
  }
  
  public static void Orgnize(){
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(4);
    String subject = Thesis();
    for(int i = 0; i < randomInt; i++){
      SecondSentence(subject);
    }
    Conclusion(subject);
  }
  
  public static void main(String[] args){
    Orgnize();
  }
}