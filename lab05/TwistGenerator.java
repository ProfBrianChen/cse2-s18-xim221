// Xinchen Ma
// March. 2 2018
// CSE02
/*TwistGenerator:First, ask the user for a positive integer called length.
If the user does not provide an integer, or if the integer is not positive, then ask again.
finally print out the result. */

//import Scanner class
import java.util.Scanner;

class TwistGenerator{
  public static void main(String[] args){
    Scanner myScan = new Scanner(System.in); //create a new object in Scanner class
    
    int length = -1; //initiate length
    String test = ""; //initiate test
    
    //detect what users typed in: only integers will be accepted
    boolean determine = true;
    while(determine){
      System.out.print("Please type the length: ");
      if(!myScan.hasNextInt()){
        determine = true;
        String junkWord = myScan.next();
      }//end of if
      else if((length = myScan.nextInt()) < 1){
        determine = true;
      }//end of elseif
      else{
        determine = false;
      }//end of else
    }//end of while
    
    int count = length / 3; // initiate the count
    int exCount = 0; //initiate the exCount
    
    //determine the numbers of extra lines
    exCount = length % 3;
    
    //print out the first full repeats"\ /"
    while(count > 0){
      System.out.print("\\ /");
      count--;
    }//end of while loop
    
    count = length / 3;// reset count
    
    //print out the extre parts
    switch(exCount){
      case 1:
        System.out.println("\\");
        break;
        
      case 2:
        System.out.println("\\ ");
        break;
        
      default:
        System.out.println("");
        break;
    }//end of switch
    
    //print out the sencond full repeats" X "
    while(count > 0){
      System.out.print(" X ");
      count--;
    }//end of while loop
    
    //print out the extre parts
    switch(exCount){
      case 1:
        System.out.println(" ");
        break;
        
      case 2:
        System.out.println(" X");
        break;
        
      default:
        System.out.println("");
        break;
    }//end of switch
    
    count = length / 3; //reset count
    
    //print out the last full repeats"/ \"
    while(count > 0){
      System.out.print("/ \\");
      count--;
    }//end of while loop
   
    
    //print out the extre parts
    switch(exCount){
      case 1:
        System.out.println("/");
        break;
        
      case 2:
        System.out.println("/ ");
        break;
        
      default:
        System.out.println("");
        break;
    }//end of switch
  }
}