//Xinchen Ma
//April. 4  2018
//CSE02
/*
 * Hw05:ask users for serval input: determine if the types of input are correct, 
 * if not, ask uses to retype
 */

import java.util.Scanner;

public class CSE2Linear {
	public static void main(String[] args) {

		//initiate variables and objects
		int[] grade = new int[15];
		int index = 0;
		boolean determine = true;
		int trash = -1;
		Scanner myScan = new Scanner(System.in);

		//Check input
		while(determine) {
			System.out.print("Please enter a integer: ");
			if(!myScan.hasNextInt()){
				determine = true;
				String junkWord = myScan.next();
				System.out.println("This is not a integer.");
			}
			else if((trash = myScan.nextInt()) < 1 || trash > 100){
				determine = true;
				System.out.println("This integer is out of range(0 ~ 100).");
			}//end of elseif
			else if(index > 0) {
				if(trash < grade[index - 1]) {
					determine = true;
					System.out.println("This integer should bigger than or equal to the last integer");
				}

				else{
					grade[index] = trash;
					index++;
					if(index == 15) {
						break;
					}
				}//end of else
			}//end of while
			else {
				grade[0] = trash;
				index++;
			}
		}

		//print out input
		System.out.println("The 15 ascending ints for final grades in CSE2 is: ");
		for(int j = 0; j < 15; j ++) {

			System.out.print(grade[j] + " ");
		}//end of print input
		System.out.println();

		determine = true;


		//search the grade
		//read the input
		int checkGrade = -1;

		while(determine) {
			System.out.print("Enter a grade to search for: ");
			if(!myScan.hasNextInt()){
				determine = true;
				String junkWord2 = myScan.next();
				System.out.println("This is not a integer.");
			}
			else{
				checkGrade = myScan.nextInt();
				determine = false;
			}
		}

		//use the binary search to find the specific grade
		binarySearch(grade, checkGrade);
		
		//scrambling
		int[] newArray = scrambling(grade);
		System.out.println("Scrambled: ");
		for(int i = 0; i < 14; i++) {
			System.out.print(newArray[i] + " ");
		}
		System.out.println();

		//reread the input
		int checkGradeLinear = -1;
		determine = true;
		while(determine) {
			System.out.print("Enter a grade to search for: ");
			if(!myScan.hasNextInt()){
				determine = true;
				String junkWord3 = myScan.next();
				System.out.println("This is not a integer.");
			}
			else{
				checkGradeLinear = myScan.nextInt();
				determine = false;
			}
		}
		
		//linear search
		linearSearch(newArray, checkGradeLinear);

	}//end of main method

	//binary search method
	public static void binarySearch(int[] a, int target) {
		int high = a.length - 1;
		int low = 0;
		int iterations = 0;
		boolean determine = false;
		while (high >= low) {
			int mid = (high + low) / 2;
			if (target > a[mid]) {
				low = mid + 1;
			}
			else if (target < a[mid]) {
				high = mid - 1;
			}
			else{				
				iterations++;
				determine = true;
				break;
			}
			iterations++;
		}
		if(determine) {
			iterations++;
			System.out.println(target + " was found with " + iterations + " iterations");
		}
		else {
			System.out.println(target + " was not found with " + iterations + " iterations");
		}
	}

	/*random scrambling:
	 * disorder the serial number of the original array
	 */
	public static int[] scrambling(int[] a) {
		int arrayLength = a.length;
		int[] newArray = new int[arrayLength];
		for(int i = 0; i < arrayLength; i++) {
			boolean determine = true;
			while(determine) {
				int position = (int)(Math.random() * arrayLength);
				if(a[position] != 0) {
					newArray[i] = a[position];
					a[position] = 0;
					determine = false;
				}
			}
		}
		return newArray;
	}

	//linear search
	public static void linearSearch(int[] a, int target) {
		int i = 0;
		boolean determine = false;
		int iterations = 0;
		for(i = 0; i < a.length; i++) {
			if(a[i] == target) {
				iterations = i;
				determine = true;
				break;
			}
		}

		if(determine) {
			System.out.println(target + " was found with " + iterations + " iterations");
		}
		else {
			System.out.println(target + " was not found with 15 iterations");
		}
	}
}