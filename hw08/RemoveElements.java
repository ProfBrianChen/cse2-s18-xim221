//Xinchen Ma
//April. 4  2018
//CSE02
/*
 * RemoveElements: create three methods: randomInput, delete, remove
 */

import java.util.Scanner;

public class RemoveElements{

	//randomInput: fill a array which length is 9 with random number
	public static int[] randomInput() {
		int[] array = new int[10];
		for(int i = 0; i < 10; i++) {
			array[i] = (int)(Math.random() * 10);
		}
		return array;
	}

	//delete: delete a position in the array
	public static int[] delete(int[] a, int pos) {
		int[] array = new int[a.length - 1];
		for(int i = 0; i < pos;i++) {
			array[i] = a[i];
		}
		for(int i = pos; i < 9;i++) {
			array[i] = a[i + 1];
		}
		return array;
	}

	//remove: remove all the value which equals to the input
	public static int[] remove(int[] a, int val) {
		int[] copy = a;
		int[] positions = new int[10];
		int index = 0;
		int length = 0;
		boolean determine = true;
		for(int i = 0; i < 10; i++) {
			for(int j = 0; j < 10; j++) {
				if(copy[j] == val) {
					positions[i] = j;
					copy[j] = -1;
					break;
				}
			}
		}
		while(determine) {
			if(positions[index] != 0) {
				length++;
				index++;
			}
			else {
				determine = false;
			}
		}
		int[] result = new int[10 - length];
		for(int i = 0; i < 10 - length; i++) {
			for(int j = 0; j < 10; j++) {
				if(copy[j] > -1) {
					result[i] = copy[j];
					copy[j] = -1;
					break;
				}
			}
		}
		return result;
	}

	public static void main(String[] args){
		Scanner scan=new Scanner(System.in);
		int num[]=new int[10];
		int newArray1[];
		int newArray2[];
		int index,target;
		String answer="";
		do{
			System.out.print("Random input 10 ints [0-9]");
			num = randomInput();
			String out = "The original array is:";
			out += listArray(num);
			System.out.println(out);

			System.out.print("Enter the index ");
			index = scan.nextInt();
			newArray1 = delete(num,index);
			String out1="The output array is ";
			out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
			System.out.println(out1);

			System.out.print("Enter the target value ");
			target = scan.nextInt();
			newArray2 = remove(num,target);
			String out2="The output array is ";
			out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
			System.out.println(out2);

			System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
			answer=scan.next();
		}while(answer.equals("Y") || answer.equals("y"));
	}

	public static String listArray(int num[]){
		String out="{";
		for(int j=0;j<num.length;j++){
			if(j>0){
				out+=", ";
			}
			out+=num[j];
		}
		out+="} ";
		return out;
	}
}