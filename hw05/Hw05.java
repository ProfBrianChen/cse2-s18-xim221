//Xinchen Ma
//March. 6 2018
//CSE02
/*
 * Hw05:ask users for serval input: determine if the types of input are correct, 
 * if not, ask uses to retype
*/

import java.util.Scanner;//import scanner class

public class Hw05 {
	public static void main (String[] args) {
		
		Scanner myScanner = new Scanner(System.in);//creat a new Scanner obj
		
		//initiate variables
		int courseNum = 0;
		int numOfTimes = 0;
		int numOfStudents = 0;
		String departmentName = "";
		String instructorName = "";
		String startTime = "";
		String junkWord = "";
		
		//ask for course number
		System.out.print("Please type your course number: ");
		while(!myScanner.hasNextInt()) {
			System.out.print("Please type a integral number: ");
			junkWord = myScanner.next();
		}//end of while
		courseNum = myScanner.nextInt();
		
		//ask for the department name
		System.out.print("Please type your department name: ");
		while(myScanner.hasNextDouble() || myScanner.hasNextInt()) {
			System.out.print("Please type a string: ");
			junkWord = myScanner.next();
		}//end of while
		departmentName = myScanner.next();
		
		//ask for the number of times it meets in a week
		System.out.print("Please type your number of times it meets in a week: ");
		while(!myScanner.hasNextInt()) {
			System.out.print("Please type a integral number: ");
			junkWord = myScanner.next();
		}//end of while
		numOfTimes = myScanner.nextInt();
		
		//ask for the time the class starts
		System.out.print("Please type your class start time: ");
		while(myScanner.hasNextDouble() || myScanner.hasNextInt()) {
			System.out.print("Please type a string: ");
			junkWord = myScanner.next();
		}//end of while
		startTime = myScanner.next();
		
		//ask for the instructor name
		System.out.print("Please type your instructor name: ");
		while(myScanner.hasNextDouble() || myScanner.hasNextInt()) {
			System.out.print("Please type a string: ");
			junkWord = myScanner.next();
		}//end of while
		instructorName = myScanner.next();
		
		//ask for the number of students
		System.out.print("Please type number of students: ");
		while(!myScanner.hasNextInt()) {
			System.out.print("Please type a integral number: ");
			junkWord = myScanner.next();
		}//end of while
		numOfStudents = myScanner.nextInt();
		
		//print out
		System.out.println("\n\nThe course number: " + courseNum);
		System.out.println("The department name: " + departmentName);
		System.out.println("The number of times it meets in a week: " + numOfTimes);
		System.out.println("The time the class starts: " + startTime);
		System.out.println("The instructor name: " + instructorName);
		System.out.println("The number of students: " + numOfStudents);
		
	}//end of main method
}//end of class
