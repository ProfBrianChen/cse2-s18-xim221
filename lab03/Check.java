// Xinchen Ma
// Feb. 9 2018
// CSE02
// Check: calculate input numbers 

import java.util.Scanner; //import Scanner class

public class Check{
  public static void main(String[] args){
    
    //Read checkcost
    Scanner myScanner = new Scanner(System.in); //accept input
    System.out.print("Enter the original cost of the check in the form XX.XX: "); 
    double checkCost = myScanner.nextDouble();  //give the value to checkCost
    
    //Read tip percentage
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form XX): ");
    double tipPercent = myScanner.nextDouble(); //give the value to the tipPercent
    tipPercent /= 100; //convert the percentage into a decimal value
    
    //Read number of people
    System.out.print("Enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt(); //give the value to numPeople
    
    //Calculate and print out the output
    double totalCost;
    double coustPerPerson;
    int dollars, dimes, pennies;
    
    totalCost = checkCost * (1 + tipPercent);
    coustPerPerson = totalCost / numPeople;
    
    dollars = (int)coustPerPerson; // get the whole amount, dropping decimal fraction
    dimes = (int)(coustPerPerson * 10) % 10; //get dimes amount
    pennies = (int)(coustPerPerson * 100) % 10;
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);
  } //end of main method
} //end of class